import { useState } from 'react'
import { Formik, Form, Field } from 'formik'
import axios from 'axios'
import './header.css'
import './content.css'
import './article.css'

const App = () => {
  const [photos, setPhotos] = useState([])
  const open = url => window.open(url)
  console.log({photos})
  return (
    <div>
      <header>
        <Formik
          initialValues={{search: ''}}
          onSubmit={async ({search}) => {
            try {
              const {data:{results}} = await axios.get(`https://api.unsplash.com/search/photos?per_page=20&query=${search}`,
              {
                headers: {
                  'Authorization': 'Client-ID NUcSovA98xovwjDhCK5dox8mz-BvGZ3szt3FCpTttO0'
                }
              })
              setPhotos(results)
            } catch (error) {
              if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
              } else if (error.request) {
                console.log(error.request);
              } else {
                console.log('Error', error.message);
              }
            }            
          }}
        >
          <Form>
            <Field name='search' />
          </Form>
        </Formik>
      </header>
      <div className='container'>
        <div className='center'>
          {photos.map(({id, links: {html}, urls:{regular}, description, alt_description})=>
            <article key={id} onClick={() => open(html)}>
              <img src={regular} loading="lazy" />
              <p>{`${description ? `${description} - ` : ''}${alt_description}`}</p>
            </article>
            )}
        </div>
      </div>
    </div>
  )
}

export default App;
